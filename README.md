# My first REST API with nodejs

## Run server localy

yarn devStart

[http://localhost:3000/](http://localhost:3000/)

## Build and run the image

$ docker build -t my-first-nodejs-rest-api .

$ docker run --rm --name my-first-nodejs-rest-api  -d -it -p 3000:3000 my-first-nodejs-rest-api:latest


