# Base Image
FROM node:10
# Create app directory
WORKDIR /usr/src/app
# Install app dependencies
COPY package*.json ./
RUN npm install
# Bundle app source
COPY . .
#Expose port to 3000
EXPOSE 3000
# Add command to run
CMD [ "node", "server.js" ]